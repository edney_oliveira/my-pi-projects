**Bem vindo!**

Baixe o App atualizado que já instalada o servidor no rpi: https://play.google.com/store/apps/details?id=br.com.housepi


------------------------------------------------------------------------------------

**Se for utilizar, referencie o autor!**


Os fontes do aplicativo servidor encontram-se na pasta Servidor, o mesmo foi desenvolvido em Python utilizando a WebIDE da Adafruit.

Já os do aplicativo mobile encontram-se na pasta Android e foi desenvolvido em Java utilizando o Eclipse Juno.

Na pasta documentos encontram-se alguns arquivos que podem ser úteis.

Na pasta Instaladores será possível encontrar a versão compilada dos aplicativos.

Imagens:

![Screenshot_2014-05-17-14-07-11.png](https://bitbucket.org/repo/KbG8KA/images/2324044091-Screenshot_2014-05-17-14-07-11.png)![20140509_003926_Android.jpg](https://bitbucket.org/repo/KbG8KA/images/2019319887-20140509_003926_Android.jpg)